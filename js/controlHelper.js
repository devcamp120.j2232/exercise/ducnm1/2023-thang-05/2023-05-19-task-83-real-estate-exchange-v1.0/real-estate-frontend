export function setDatasourceSelectElement(elementSelector, dataSource, valueField, displayField, defaultValue) {
    let element = document.querySelector(elementSelector);
    //check element exists
    if (!element) {
        alert("Element not found");
        return;
    }
    // else{
    //     console.log("Element found");
    //     console.log(element);
    // }

    element.innerHTML = "";

    for (let i = 0; i < dataSource.length; i++) {
        let option = document.createElement("option");
        option.value = dataSource[i][valueField];
        option.innerHTML = dataSource[i][displayField];
        element.append(option);
    }

    if (defaultValue) {
        element.value = defaultValue;
    }
    // else {
    //     //set default value is the first item
    //     console.log("set default value is the first item");
    //     element.value = dataSource[0][valueField];
    // }
}
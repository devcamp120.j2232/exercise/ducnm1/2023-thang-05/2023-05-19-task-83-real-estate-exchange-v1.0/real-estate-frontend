export const INPUTTYPE = {
    INPUT_TEXT: "input",
    INPUT_FILE: "file",
    SELECT: "select",
    SELECT_MULTIPLE: "select-multiple",
    PASSWORD: "password",
    TEXTAREA: "textarea",
    DATE: "datetime",
    CHECKBOX: "checkbox"
};

export class CustomModal {
    constructor(title) {
        this.title = title;

        let md = document.getElementById("modalWrapper");
        if (md) {
            md.remove();
        }

        this.modalWrapper = document.createElement("div");
        this.modalWrapper.id = "modalWrapper";
        this.modalWrapper.innerHTML = `
        <div class="modal fade" id="customModal1" tabindex="-1" aria-labelledby="customModal1Label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="customModal1Label">${this.title}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <div class="modal-body">
                    
                </div>
                    <div class="modal-footer">
                        <button id="close" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        `;
        document.body.append(this.modalWrapper);
    }

    show() {
        var modal = new bootstrap.Modal(
            this.modalWrapper.querySelector("#customModal1")
        );
        modal.show();
    }

    hide() {
        this.modalWrapper.querySelector("#close").click();
    }

    addBodyText(text) {
        let modalBody = this.modalWrapper.querySelector(".modal-body");
        let p = document.createElement("p");
        p.innerHTML = text;
        modalBody.append(p);
    }

    addBodyRow(labelText, inputType, inputId, value) {
        let modalBody = this.modalWrapper.querySelector(".modal-body");
        //create a div with class row form-group
        let div = document.createElement("div");
        div.classList.add("row", "form-group");
        modalBody.append(div);

        //add label
        //create a div container for label with class col-4
        let divLabel = document.createElement("div");
        divLabel.classList.add("col-4");
        div.append(divLabel);

        let label = document.createElement("label");
        label.for = inputId;
        label.classList.add("col-form-label");
        label.innerHTML = labelText;
        divLabel.append(label);

        //create a div container for input with class col-8
        let divInput = document.createElement("div");
        divInput.type = inputType;
        divInput.classList.add("col-8");
        div.append(divInput);

        var input = null;
        if (inputType == INPUTTYPE.INPUT_TEXT) {
            input = document.createElement("input");
        }
        if (inputType == INPUTTYPE.INPUT_FILE) {
            /*
            *<div class="custom-file">
                <input type="file" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
            </div>
            */
            input = document.createElement("div");
            input.classList.add("custom-file");
            let inputFile = document.createElement("input");
            inputFile.type = "file";
            inputFile.classList.add("custom-file-input");
            inputFile.id = inputId;
            let label = document.createElement("label");
            label.classList.add("custom-file-label");
            label.for = inputId;
            label.innerHTML = "Choose file";
            input.append(inputFile);
            input.append(label);
        }
        if (inputType == INPUTTYPE.SELECT) {
            input = document.createElement("select");
        }
        if (inputType == INPUTTYPE.SELECT_MULTIPLE) {
            input = document.createElement("select");
            input.classList.add("select2");
            input.multiple = true;
        }
        if (inputType == INPUTTYPE.PASSWORD) {
            input = document.createElement("input");
            input.type = "password";
        }
        if (inputType == INPUTTYPE.TEXTAREA) {
            input = document.createElement("textarea");
        }
        if (inputType == INPUTTYPE.DATE) {
            input = document.createElement("input");
            input.type = "date";
        }
        if (inputType == INPUTTYPE.CHECKBOX) {
            input = document.createElement("input");
            input.type = "checkbox";
        }

        input.id = inputId;
        if (inputType != INPUTTYPE.INPUT_FILE) {
            input.classList.add("form-control");
        }
        // input.classList.add("form-control");
        divInput.append(input);

        //add value if exists
        if (value) {
            try {
                if (inputType == INPUTTYPE.SELECT_MULTIPLE) {
                    let values = [];
                    let valueArray = value.split(",");
                    for (let i = 0; i < valueArray.length; i++) {
                        values.push(valueArray[i].trim());
                    }
                    console.log(input);
                    console.log($(input));
                    $(input).val(values);
                    $(input).trigger("change");
                    console.log("trigger change");
            
                } else {
                    input.value = value;
                }
            } catch (error) {
                console.error("error when set value in custom modal", error);
            }
        }
        if (inputType == INPUTTYPE.SELECT_MULTIPLE) {
            $(input).select2();
        }
    }

    addFooterButton(content, classList, callback) {
        let modalFooter = this.modalWrapper.querySelector(".modal-footer");
        let button = document.createElement("button");
        button.type = "button";

        let classListArray = classList.split(" ");
        for (let i = 0; i < classListArray.length; i++) {
            button.classList.add(classListArray[i]);
        }

        //button.id = id;
        button.innerHTML = content;
        button.addEventListener("click", callback);

        //append before the close button
        modalFooter.insertBefore(button, modalFooter.firstChild);
    }
}

export function closeModal(selector) {
    document.querySelector(selector).querySelector("#close").click();
}



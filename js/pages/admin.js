$(document).ready(function () {
    $("#address-map").on("click", function () { addPageScript("../js/pages/addressMap.js"); });
});

function addPageScript(link) {
    //remove script with src if exists
    var script = document.querySelector("script[src='" + link + "']");
    if (script != null) {
        script.remove();
    }

    var script = document.createElement("script");
    script.src = link;
    script.type = "module";

    document.body.appendChild(script);
}


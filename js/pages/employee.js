import * as adminLteHelper from '../adminlte-utils.js';
import * as dataTableHelper from '../datatable-utils.js';
import * as CM from "../CustomModal.js"
import * as responseHandler from "../responseHandler.js"

const ACTIVE_MENU_CONTENT = "Người dùng";
const DOMAIN = "http://localhost:8080";
const BASE_URL_API = DOMAIN + "/employee";
var SELECTED_OBJECT = null;

$(document).ready(function () {
    setContent();
    addEventListener();
    initDataTable();
    loadDataFromDatabase();
});

function setContent() {
    adminLteHelper.setActiveMenu(ACTIVE_MENU_CONTENT);
    adminLteHelper.setActiveBreadcrumbItemContent(ACTIVE_MENU_CONTENT);
    adminLteHelper.setContentHeader(ACTIVE_MENU_CONTENT);
}

function addEventListener() {
    $("#btn-show-modal-addnew").on("click", function () { showModalAddNew(); });
    $("#table").on("click", ".action-edit", function () { showModalEdit(this); });
    $("#table").on("click", ".action-delete", function () { showModalDelete(this); });
    $("#btn-show-modal-delete-all").on("click", function () { showModalDeleteAll(); });
}

function initDataTable() {
    $("#table").DataTable({
        columns: [
            { data: "id", defaultContent: "" },
            { data: "lastName", defaultContent: "" },
            { data: "firstName", defaultContent: "" },
            { data: "title", defaultContent: "" },
            { data: "titleOfCourtesy", defaultContent: "" },
            { data: "birthDate", defaultContent: "" },
            { data: "hireDate", defaultContent: "" },
            { data: "address", defaultContent: "" },
            { data: "city", defaultContent: "" },
            { data: "region", defaultContent: "" },
            { data: "postalCode", defaultContent: "" },
            { data: "country", defaultContent: "" },
            { data: "homePhone", defaultContent: "" },
            { data: "extension", defaultContent: "" },
            { data: "photo", defaultContent: "" },
            { data: "notes", defaultContent: "" },
            { data: "reportsTo", defaultContent: "" },
            { data: "username", defaultContent: "" },
            { data: "password", defaultContent: "" },
            { data: "email", defaultContent: "" },
            { data: "activated", defaultContent: "" },
            { data: "profile", defaultContent: "" },
            { data: "userLevel", defaultContent: "" },
            {
                defaultContent:
                    `
                    <i class="fa fa-edit action-edit" style="cursor:pointer;color:blue;margin-right:10px"></i>
                    <i class="fa fa-trash action-delete" style="cursor:pointer;color:red"></i>
                    `
            }
        ]
    });
}

function loadDataFromDatabase() {
    fetch(BASE_URL_API + "/all")
        .then(response => response.json())
        .then(data => {
            console.log(data);
            dataTableHelper.setDatasource($("#table").DataTable(), data.body);
        })
        .catch(error => {
            console.error(error);
        });
}

function showModalAddNew() {
    var cm = new CM.CustomModal("Thêm mới");
    cm.addBodyRow("Lastname", CM.INPUTTYPE.INPUT_TEXT, "lastName");
    cm.addBodyRow("Firstname", CM.INPUTTYPE.INPUT_TEXT, "firstName");
    cm.addBodyRow("Title", CM.INPUTTYPE.INPUT_TEXT, "title");
    cm.addBodyRow("Title of courtesy", CM.INPUTTYPE.INPUT_TEXT, "titleOfCourtesy");
    cm.addBodyRow("Birth date", CM.INPUTTYPE.DATE, "birthDate");
    cm.addBodyRow("Hire date", CM.INPUTTYPE.DATE, "hireDate");
    cm.addBodyRow("Address", CM.INPUTTYPE.INPUT_TEXT, "address");
    cm.addBodyRow("City", CM.INPUTTYPE.INPUT_TEXT, "city");
    cm.addBodyRow("Region", CM.INPUTTYPE.INPUT_TEXT, "region");
    cm.addBodyRow("Postal code", CM.INPUTTYPE.INPUT_TEXT, "postalCode");
    cm.addBodyRow("Country", CM.INPUTTYPE.INPUT_TEXT, "country");
    cm.addBodyRow("Home phone", CM.INPUTTYPE.INPUT_TEXT, "homePhone");
    cm.addBodyRow("Extension", CM.INPUTTYPE.INPUT_TEXT, "extension");
    cm.addBodyRow("Photo", CM.INPUTTYPE.INPUT_FILE, "photo");
    cm.addBodyRow("Notes", CM.INPUTTYPE.TEXTAREA, "notes");
    cm.addBodyRow("Reports to", CM.INPUTTYPE.INPUT_TEXT, "reportsTo");
    cm.addBodyRow("Username", CM.INPUTTYPE.INPUT_TEXT, "username");
    cm.addBodyRow("Password", CM.INPUTTYPE.PASSWORD, "password");
    cm.addBodyRow("Email", CM.INPUTTYPE.INPUT_TEXT, "email");
    cm.addBodyRow("Activated", CM.INPUTTYPE.SELECT, "activated");
    cm.addBodyRow("Profile", CM.INPUTTYPE.TEXTAREA, "profile");
    cm.addBodyRow("User level", CM.INPUTTYPE.INPUT_TEXT, "userLevel");

    cm.addFooterButton("Thêm mới", "btn btn-success", function () { createMethod(); });

    cm.show();
    addDatasourceAndSetValueSelectElement("#active");
}

async function showModalEdit(sender) {
    let selectedRow = $(sender).parents("tr");
    let selectedObject = $("#table").DataTable().row(selectedRow).data();
    SELECTED_OBJECT = selectedObject;
    console.log(selectedObject);

    var cm = new CM.CustomModal("Chi tiết");
    cm.addBodyRow("Lastname", CM.INPUTTYPE.INPUT_TEXT, "lastName", selectedObject.lastName);
    cm.addBodyRow("Firstname", CM.INPUTTYPE.INPUT_TEXT, "firstName", selectedObject.firstName);
    cm.addBodyRow("Title", CM.INPUTTYPE.INPUT_TEXT, "title", selectedObject.title);
    cm.addBodyRow("Title of courtesy", CM.INPUTTYPE.INPUT_TEXT, "titleOfCourtesy", selectedObject.titleOfCourtesy);
    cm.addBodyRow("Birth date", CM.INPUTTYPE.DATE, "birthDate", selectedObject.birthDate);
    cm.addBodyRow("Hire date", CM.INPUTTYPE.DATE, "hireDate", selectedObject.hireDate);
    cm.addBodyRow("Address", CM.INPUTTYPE.INPUT_TEXT, "address", selectedObject.address);
    cm.addBodyRow("City", CM.INPUTTYPE.INPUT_TEXT, "city", selectedObject.city);
    cm.addBodyRow("Region", CM.INPUTTYPE.INPUT_TEXT, "region", selectedObject.region);
    cm.addBodyRow("Postal code", CM.INPUTTYPE.INPUT_TEXT, "postalCode", selectedObject.postalCode);
    cm.addBodyRow("Country", CM.INPUTTYPE.INPUT_TEXT, "country", selectedObject.country);
    cm.addBodyRow("Home phone", CM.INPUTTYPE.INPUT_TEXT, "homePhone", selectedObject.homePhone);
    cm.addBodyRow("Extension", CM.INPUTTYPE.INPUT_TEXT, "extension", selectedObject.extension);
    cm.addBodyRow("Photo", CM.INPUTTYPE.INPUT_FILE, "photo", selectedObject.photo);
    cm.addBodyRow("Notes", CM.INPUTTYPE.TEXTAREA, "notes", selectedObject.notes);
    cm.addBodyRow("Reports to", CM.INPUTTYPE.INPUT_TEXT, "reportsTo", selectedObject.reportsTo);
    cm.addBodyRow("Username", CM.INPUTTYPE.INPUT_TEXT, "username", selectedObject.username);
    cm.addBodyRow("Password", CM.INPUTTYPE.PASSWORD, "password", selectedObject.password);
    cm.addBodyRow("Email", CM.INPUTTYPE.INPUT_TEXT, "email", selectedObject.email);
    cm.addBodyRow("Activated", CM.INPUTTYPE.SELECT, "activated", selectedObject.activated);
    cm.addBodyRow("Profile", CM.INPUTTYPE.TEXTAREA, "profile", selectedObject.profile);
    cm.addBodyRow("User level", CM.INPUTTYPE.INPUT_TEXT, "userLevel", selectedObject.userLevel);

    cm.addFooterButton("Lưu lại", "btn btn-primary", function () { updateMethod(selectedObject.id); });

    cm.show();
    addDatasourceAndSetValueSelectElement("#activated");
}

function showModalDelete(sender) {
    let selectedRow = $(sender).parents("tr");
    let selectedObject = $("#table").DataTable().row(selectedRow).data();

    var cm = new CM.CustomModal("Xóa");
    cm.addBodyText("Bạn có chắc chắn muốn xóa?");
    cm.addFooterButton("Xóa", "btn btn-danger", function () { deleteMethod(selectedObject.id); });
    cm.show();
}

function showModalDeleteAll() {
    var cm = new CM.CustomModal("Xóa tất cả");
    cm.addBodyText("Bạn có chắc chắn muốn xóa tất cả?");
    cm.addFooterButton("Xóa", "btn btn-danger", function () { deleteAllMethod(); });
    cm.show();
}

function addDatasourceAndSetValueSelectElement(elementSelector) {
    let data = [
        { value: "Y", text: "Yes" },
        { value: "N", text: "No" }
    ];

    var selectElement = $("#activated");
    data.forEach(element => {
        $("<option>").val(element.value).text(element.text).appendTo(selectElement);
    });

    if (SELECTED_OBJECT != null) {
        $(elementSelector).val(SELECTED_OBJECT.activated);
    }
}

function getDataObjectFromModal() {
    let data = {
        lastName: $("#lastName").val(),
        firstName: $("#firstName").val(),
        title: $("#title").val(),
        titleOfCourtesy: $("#titleOfCourtesy").val(),
        birthDate: $("#birthDate").val(),
        hireDate: $("#hireDate").val(),
        address: $("#address").val(),
        city: $("#city").val(),
        region: $("#region").val(),
        postalCode: $("#postalCode").val(),
        country: $("#country").val(),
        homePhone: $("#homePhone").val(),
        extension: $("#extension").val(),
        photo: $("#photo").val(),
        notes: $("#notes").val(),
        reportsTo: $("#reportsTo").val(),
        username: $("#username").val(),
        password: $("#password").val(),
        email: $("#email").val(),
        activated: $("#activated").val(),
        profile: $("#profile").val(),
        userLevel: $("#userLevel").val(),
    }

    return data;
}

async function createMethod() {
    let requestObject = getDataObjectFromModal();
    let projectId = $("#project-id").val();
    console.log(requestObject);

    let response = await fetch(BASE_URL_API + "/create", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(requestObject),
    });

    let data = await response.json();
    if (response.ok) {
        console.log(data);
        alert(data.message);
        loadDataFromDatabase();
        CM.closeModal("#customModal1");
    }
    else {
        console.log(data);
        alert(data.message + "\n" + responseHandler.getErrorFromResponse(data));
    }
}

async function updateMethod(id) {
    let requestObject = getDataObjectFromModal();
    let projectId = $("#project-id").val();
    console.log(requestObject);

    let response = await fetch(BASE_URL_API + "/update/" + id, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(requestObject),
    });

    let data = await response.json();
    if (response.ok) {
        console.log(data);
        alert(data.message);
        loadDataFromDatabase();
        CM.closeModal("#customModal1");
    }
    else {
        console.log(data);
        alert(data.message + "\n" + responseHandler.getErrorFromResponse(data));
    }
}

function deleteMethod(id) {
    fetch(BASE_URL_API + "/delete/" + id, {
        method: "DELETE",
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            alert(data.message);
            loadDataFromDatabase();
            CM.closeModal("#customModal1");
        })
        .catch(error => {
            console.error(error);
        });
}

function deleteAllMethod() {
    fetch(BASE_URL_API + "/deleteall", {
        method: "DELETE",
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            alert(data.message);
            loadDataFromDatabase();
            CM.closeModal("#customModal1");
        })
        .catch(error => {
            console.error(error);
        });
}

import * as adminLteHelper from '../adminlte-utils.js';
import * as dataTableHelper from '../datatable-utils.js';
import * as CM from "../CustomModal.js"
import * as responseHandler from "../responseHandler.js"

const ACTIVE_MENU_CONTENT = "Đơn vị thiết kế";
const DOMAIN = "http://localhost:8080";
const BASE_URL_API = DOMAIN + "/designunit";
var SELECTED_OBJECT = null;

$(document).ready(function () {
    setContent();
    addEventListener();
    initDataTable();
    loadDataFromDatabase();
});

function setContent() {
    adminLteHelper.setActiveMenu(ACTIVE_MENU_CONTENT);
    adminLteHelper.setActiveBreadcrumbItemContent(ACTIVE_MENU_CONTENT);
    adminLteHelper.setContentHeader(ACTIVE_MENU_CONTENT);
}

function addEventListener() {
    $("#btn-show-modal-addnew").on("click", function () { showModalAddNew(); });
    $("#table").on("click", ".action-edit", function () { showModalEdit(this); });
    $("#table").on("click", ".action-delete", function () { showModalDelete(this); });
    $("#btn-show-modal-delete-all").on("click", function () { showModalDeleteAll(); });
}

function initDataTable() {
    $("#table").DataTable({
        columns: [
            { data: "id", defaultContent: "" },
            { data: "name", defaultContent: "" },
            { data: "description", defaultContent: "" },
            { data: "projects", defaultContent: "" },
            { data: "address.address", defaultContent: "" },
            { data: "phone", defaultContent: "" },
            { data: "phone2", defaultContent: "" },
            { data: "fax", defaultContent: "" },
            { data: "email", defaultContent: "" },
            { data: "website", defaultContent: "" },
            { data: "note", defaultContent: "" },
            {
                defaultContent:
                    `
                    <i class="fa fa-edit action-edit" style="cursor:pointer;color:blue;margin-right:10px"></i>
                    <i class="fa fa-trash action-delete" style="cursor:pointer;color:red"></i>
                    `
            }
        ]
    });
}

function loadDataFromDatabase() {
    fetch(BASE_URL_API + "/all")
        .then(response => response.json())
        .then(data => {
            dataTableHelper.setDatasource($("#table").DataTable(), data.body);
        })
        .catch(error => {
            console.error(error);
        });
}

function showModalAddNew() {
    var cm = new CM.CustomModal("Thêm mới");
    cm.addBodyRow("Tên", CM.INPUTTYPE.INPUT_TEXT, "name");
    cm.addBodyRow("Mô tả", CM.INPUTTYPE.TEXTAREA, "description");
    cm.addBodyRow("Dự án đã làm", CM.INPUTTYPE.INPUT_TEXT, "projects");
    cm.addBodyRow("Địa chỉ", CM.INPUTTYPE.SELECT, "address");
    cm.addBodyRow("Số điện thoại", CM.INPUTTYPE.INPUT_TEXT, "phone");
    cm.addBodyRow("Số điện thoại 2", CM.INPUTTYPE.INPUT_TEXT, "phone2");
    cm.addBodyRow("Fax", CM.INPUTTYPE.INPUT_TEXT, "fax");
    cm.addBodyRow("Email", CM.INPUTTYPE.INPUT_TEXT, "email");
    cm.addBodyRow("Website", CM.INPUTTYPE.INPUT_TEXT, "website");
    cm.addBodyRow("Note", CM.INPUTTYPE.TEXTAREA, "note");

    cm.addFooterButton("Thêm mới", "btn btn-success", function () { createMethod(); });

    addDatasourceAndSetValueSelectElement("#address");
    cm.show();
}

async function showModalEdit(sender) {
    let selectedRow = $(sender).parents("tr");
    let selectedObject = $("#table").DataTable().row(selectedRow).data();
    SELECTED_OBJECT = selectedObject;

    var cm = new CM.CustomModal("Chi tiết");
    cm.addBodyRow("Tên", CM.INPUTTYPE.INPUT_TEXT, "name", selectedObject.name);
    cm.addBodyRow("Mô tả", CM.INPUTTYPE.TEXTAREA, "description", selectedObject.description);
    cm.addBodyRow("Dự án đã làm", CM.INPUTTYPE.INPUT_TEXT, "projects", selectedObject.projects);
    cm.addBodyRow("Địa chỉ", CM.INPUTTYPE.SELECT, "address", selectedObject.address);
    cm.addBodyRow("Số điện thoại", CM.INPUTTYPE.INPUT_TEXT, "phone", selectedObject.phone);
    cm.addBodyRow("Số điện thoại 2", CM.INPUTTYPE.INPUT_TEXT, "phone2", selectedObject.phone2);
    cm.addBodyRow("Fax", CM.INPUTTYPE.INPUT_TEXT, "fax", selectedObject.fax);
    cm.addBodyRow("Email", CM.INPUTTYPE.INPUT_TEXT, "email", selectedObject.email);
    cm.addBodyRow("Website", CM.INPUTTYPE.INPUT_TEXT, "website", selectedObject.website);
    cm.addBodyRow("Note", CM.INPUTTYPE.TEXTAREA, "note", selectedObject.note);

    cm.addFooterButton("Lưu lại", "btn btn-primary", function () { updateMethod(selectedObject.id); });

    addDatasourceAndSetValueSelectElement("#address");

    cm.show();
}

function showModalDelete(sender) {
    let selectedRow = $(sender).parents("tr");
    let selectedObject = $("#table").DataTable().row(selectedRow).data();

    var cm = new CM.CustomModal("Xóa");
    cm.addBodyText("Bạn có chắc chắn muốn xóa?");
    cm.addFooterButton("Xóa", "btn btn-danger", function () { deleteMethod(selectedObject.id); });
    cm.show();
}

function showModalDeleteAll() {
    var cm = new CM.CustomModal("Xóa tất cả");
    cm.addBodyText("Bạn có chắc chắn muốn xóa tất cả?");
    cm.addFooterButton("Xóa", "btn btn-danger", function () { deleteAllMethod(); });
    cm.show();
}

function addDatasourceAndSetValueSelectElement(elementSelector) {
    fetch(DOMAIN + "/addressmap/all")
        .then(response => response.json())
        .then(data => {
            console.log(data);
            let selectElement = $(elementSelector);
            data.forEach(element => {
                selectElement.append(`<option value="${element.id}">${element.address}</option>`);
            });
            try {
                selectElement.val(SELECTED_OBJECT.address.id);
            } catch (error) {
                //selectElement.val(0);
            }
        })
        .catch(error => {
            console.error(error);
        });
}

function getDataObjectFromModal() {
    let data = {
        name: $("#name").val(),
        description: $("#description").val(),
        projects: $("#projects").val(),
        address: {
            id: $("#address").val()
        },
        phone: $("#phone").val(),
        phone2: $("#phone2").val(),
        fax: $("#fax").val(),
        email: $("#email").val(),
        website: $("#website").val(),
        note: $("#note").val()
    }
    return data;
}

async function createMethod() {
    let requestObject = getDataObjectFromModal();
    let projectId = $("#project-id").val();
    console.log(requestObject);

    let response = await fetch(BASE_URL_API + "/create", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(requestObject),
    });

    let data = await response.json();
    if (response.ok) {
        console.log(data);
        alert(data.message);
        loadDataFromDatabase();
        CM.closeModal("#customModal1");
    }
    else {
        console.log(data);
        alert(data.message + "\n" + responseHandler.getErrorFromResponse(data));
    }
}

async function updateMethod(id) {
    let requestObject = getDataObjectFromModal();
    let projectId = $("#project-id").val();
    console.log(requestObject);

    let response = await fetch(BASE_URL_API + "/update/" + id, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(requestObject),
    });

    let data = await response.json();
    if (response.ok) {
        console.log(data);
        alert(data.message);
        loadDataFromDatabase();
        CM.closeModal("#customModal1");
    }
    else {
        console.log(data);
        alert(data.message + "\n" + responseHandler.getErrorFromResponse(data));
    }
}

function deleteMethod(id) {
    fetch(BASE_URL_API + "/delete/" + id, {
        method: "DELETE",
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            alert(data.message);
            loadDataFromDatabase();
            CM.closeModal("#customModal1");
        })
        .catch(error => {
            console.error(error);
        });
}

function deleteAllMethod() {
    fetch(BASE_URL_API + "/deleteall", {
        method: "DELETE",
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            alert(data.message);
            loadDataFromDatabase();
            CM.closeModal("#customModal1");
        })
        .catch(error => {
            console.error(error);
        });
}

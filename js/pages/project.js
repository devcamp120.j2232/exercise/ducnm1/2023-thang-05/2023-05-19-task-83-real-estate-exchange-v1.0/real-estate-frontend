import * as adminLteHelper from '../adminlte-utils.js';
import * as dataTableHelper from '../datatable-utils.js';
import * as CM from "../CustomModal.js"
import * as responseHandler from "../responseHandler.js"
import * as controlHelper from "../controlHelper.js"
import * as apiHelper from "../apiHelper.js"

const ACTIVE_MENU_CONTENT = "Dự án";
const DOMAIN = "http://localhost:8080";
const BASE_URL_API = DOMAIN + "/project";
const BASE_URL_API_PROVINCE = DOMAIN + "/province";
const BASE_URL_API_DISTRICT = DOMAIN + "/district";
const BASE_URL_API_WARD = DOMAIN + "/ward";
const BASE_URL_API_STREET = DOMAIN + "/street";
const BASE_URL_API_INVESTOR = DOMAIN + "/investor";
const BASE_URL_API_CONSTRUCTION_CONTRACTOR = DOMAIN + "/constructioncontractor";
const BASE_URL_API_DESIGN_UNIT = DOMAIN + "/designunit";
const BASE_URL_API_UTILITIES = DOMAIN + "/utilities";
const BASE_URL_API_REGION_LINK = DOMAIN + "/regionlink";


var SELECTED_OBJECT = null;

$(document).ready(function () {
    setContent();
    addEventListener();
    initDataTable();
    loadDataFromDatabase();
});

function setContent() {
    adminLteHelper.setActiveMenu(ACTIVE_MENU_CONTENT);
    adminLteHelper.setActiveBreadcrumbItemContent(ACTIVE_MENU_CONTENT);
    adminLteHelper.setContentHeader(ACTIVE_MENU_CONTENT);
}

function addEventListener() {
    $("#btn-show-modal-addnew").on("click", function () { showModalAddNew(); });
    $("#table").on("click", ".action-edit", function () { showModalEdit(this); });
    $("#table").on("click", ".action-delete", function () { showModalDelete(this); });
    $("#btn-show-modal-delete-all").on("click", function () { showModalDeleteAll(); });


}

function initDataTable() {
    $("#table").DataTable({
        columns: [
            { data: "id", defaultContent: "" },
            { data: "name", defaultContent: "" },
            { data: "province.name", defaultContent: "" },
            { data: "district.name", defaultContent: "" },
            { data: "ward.name", defaultContent: "" },
            { data: "street.name", defaultContent: "" },
            { data: "address", defaultContent: "" },
            { data: "slogan", defaultContent: "" },
            { data: "description", defaultContent: "" },
            { data: "acreage", defaultContent: "" },
            { data: "constructArea", defaultContent: "" },
            { data: "numBlock", defaultContent: "" },
            { data: "numFloors", defaultContent: "" },
            { data: "numApartment", defaultContent: "" },
            { data: "apartmenttArea", defaultContent: "" },
            { data: "investor.name", defaultContent: "" },
            { data: "constructionContractor.name", defaultContent: "" },
            { data: "designUnit.name", defaultContent: "" },
            { data: "utilities", defaultContent: "" },
            { data: "regionLink", defaultContent: "" },
            { data: "photo", defaultContent: "" },
            { data: "lat", defaultContent: "" },
            { data: "lng", defaultContent: "" },
            {
                defaultContent:
                    `
                    <i class="fa fa-edit action-edit" style="cursor:pointer;color:blue;margin-right:10px"></i>
                    <i class="fa fa-trash action-delete" style="cursor:pointer;color:red"></i>
                    `
            }
        ]
    });
}

function loadDataFromDatabase() {
    fetch(BASE_URL_API + "/all")
        .then(response => response.json())
        .then(data => {
            console.log(data);
            dataTableHelper.setDatasource($("#table").DataTable(), data.body);
        })
        .catch(error => {
            console.error(error);
        });
}

function showModalAddNew() {
    var cm = new CM.CustomModal("Thêm mới");
    cm.addBodyRow("Tên dự án", CM.INPUTTYPE.INPUT_TEXT, "name");
    cm.addBodyRow("Tỉnh/Thành phố", CM.INPUTTYPE.SELECT, "province");
    cm.addBodyRow("Quận/Huyện", CM.INPUTTYPE.SELECT, "district");
    cm.addBodyRow("Phường/Xã", CM.INPUTTYPE.SELECT, "ward");
    cm.addBodyRow("Đường phố", CM.INPUTTYPE.SELECT, "street");
    cm.addBodyRow("Địa chỉ", CM.INPUTTYPE.TEXTAREA, "address");
    cm.addBodyRow("Slogan", CM.INPUTTYPE.TEXTAREA, "slogan");
    cm.addBodyRow("Mô tả", CM.INPUTTYPE.TEXTAREA, "description");
    cm.addBodyRow("Diện tích", CM.INPUTTYPE.INPUT_TEXT, "acreage");
    cm.addBodyRow("Diện tích xây dựng", CM.INPUTTYPE.INPUT_TEXT, "constructArea");
    cm.addBodyRow("Số tòa nhà", CM.INPUTTYPE.INPUT_TEXT, "numBlock");
    cm.addBodyRow("Số tầng", CM.INPUTTYPE.INPUT_TEXT, "numFloors");
    cm.addBodyRow("Số căn hộ", CM.INPUTTYPE.INPUT_TEXT, "numApartment");
    cm.addBodyRow("Diện tích căn hộ", CM.INPUTTYPE.INPUT_TEXT, "apartmenttArea");
    cm.addBodyRow("Chủ đầu tư", CM.INPUTTYPE.SELECT, "investor");
    cm.addBodyRow("Nhà thầu thi công", CM.INPUTTYPE.SELECT, "constructionContractor");
    cm.addBodyRow("Đơn vị thiết kế", CM.INPUTTYPE.SELECT, "designUnit");
    cm.addBodyRow("Tiện ích", CM.INPUTTYPE.SELECT_MULTIPLE, "utilities");
    cm.addBodyRow("Kết nối vùng", CM.INPUTTYPE.SELECT_MULTIPLE, "regionLink");
    cm.addBodyRow("Hình ảnh", CM.INPUTTYPE.INPUT_FILE, "photo");
    cm.addBodyRow("Vĩ độ", CM.INPUTTYPE.INPUT_TEXT, "lat");
    cm.addBodyRow("Kinh độ", CM.INPUTTYPE.INPUT_TEXT, "lng");

    cm.addFooterButton("Thêm mới", "btn btn-success", function () { createMethod(); });

    addDatasourceAndSetValueSelectElement();
    cm.show();
}

async function showModalEdit(sender) {
    let selectedRow = $(sender).parents("tr");
    let selectedObject = $("#table").DataTable().row(selectedRow).data();
    SELECTED_OBJECT = selectedObject;
    console.log(selectedObject);

    var cm = new CM.CustomModal("Chi tiết");
    cm.addBodyRow("Tên dự án", CM.INPUTTYPE.INPUT_TEXT, "name", selectedObject.name);
    cm.addBodyRow("Tỉnh/Thành phố", CM.INPUTTYPE.SELECT, "province", selectedObject.province);
    cm.addBodyRow("Quận/Huyện", CM.INPUTTYPE.SELECT, "district", selectedObject.district);
    cm.addBodyRow("Phường/Xã", CM.INPUTTYPE.SELECT, "ward", selectedObject.ward);
    cm.addBodyRow("Đường phố", CM.INPUTTYPE.SELECT, "street", selectedObject.street);
    cm.addBodyRow("Địa chỉ", CM.INPUTTYPE.TEXTAREA, "address", selectedObject.address);
    cm.addBodyRow("Slogan", CM.INPUTTYPE.TEXTAREA, "slogan", selectedObject.slogan);
    cm.addBodyRow("Mô tả", CM.INPUTTYPE.TEXTAREA, "description", selectedObject.description);
    cm.addBodyRow("Diện tích", CM.INPUTTYPE.INPUT_TEXT, "acreage", selectedObject.acreage);
    cm.addBodyRow("Diện tích xây dựng", CM.INPUTTYPE.INPUT_TEXT, "constructArea", selectedObject.constructArea);
    cm.addBodyRow("Số tòa nhà", CM.INPUTTYPE.INPUT_TEXT, "numBlock", selectedObject.numBlock);
    cm.addBodyRow("Số tầng", CM.INPUTTYPE.INPUT_TEXT, "numFloors", selectedObject.numFloors);
    cm.addBodyRow("Số căn hộ", CM.INPUTTYPE.INPUT_TEXT, "numApartment", selectedObject.numApartment);
    cm.addBodyRow("Diện tích căn hộ", CM.INPUTTYPE.INPUT_TEXT, "apartmenttArea", selectedObject.apartmenttArea);
    cm.addBodyRow("Chủ đầu tư", CM.INPUTTYPE.SELECT, "investor", selectedObject.investor);
    cm.addBodyRow("Nhà thầu thi công", CM.INPUTTYPE.SELECT, "constructionContractor", selectedObject.constructionContractor);
    cm.addBodyRow("Đơn vị thiết kế", CM.INPUTTYPE.SELECT, "designUnit", selectedObject.designUnit);
    cm.addBodyRow("Tiện ích", CM.INPUTTYPE.SELECT_MULTIPLE, "utilities", selectedObject.utilities);
    cm.addBodyRow("Kết nối vùng", CM.INPUTTYPE.SELECT_MULTIPLE, "regionLink", selectedObject.regionLink);
    cm.addBodyRow("Hình ảnh", CM.INPUTTYPE.INPUT_FILE, "photo", selectedObject.photo);
    cm.addBodyRow("Vĩ độ", CM.INPUTTYPE.INPUT_TEXT, "lat", selectedObject.lat);
    cm.addBodyRow("Kinh độ", CM.INPUTTYPE.INPUT_TEXT, "lng", selectedObject.lng);

    cm.addFooterButton("Lưu lại", "btn btn-primary", function () { updateMethod(selectedObject.id); });

    await addDatasourceAndSetValueSelectElement();
    cm.show();
}

function showModalDelete(sender) {
    let selectedRow = $(sender).parents("tr");
    let selectedObject = $("#table").DataTable().row(selectedRow).data();

    var cm = new CM.CustomModal("Xóa");
    cm.addBodyText("Bạn có chắc chắn muốn xóa?");
    cm.addFooterButton("Xóa", "btn btn-danger", function () { deleteMethod(selectedObject.id); });
    cm.show();
}

function showModalDeleteAll() {
    var cm = new CM.CustomModal("Xóa tất cả");
    cm.addBodyText("Bạn có chắc chắn muốn xóa tất cả?");
    cm.addFooterButton("Xóa", "btn btn-danger", function () { deleteAllMethod(); });
    cm.show();
}

async function addDatasourceAndSetValueSelectElement() {
    $("#customModal1 #province").on("change", async function () {
        await loadDistrict();
        await loadWard();
        await loadStreet();
    });
    $("#customModal1 #district").on("change", async function () {
        await loadWard();
        await loadStreet();
    });
    $("#customModal1 #ward").on("change", async function () {
        await loadStreet();
    });

    await loadProvince();
    await loadDistrict();
    await loadWard();
    await loadStreet();
    await loadInvestor();
    await loadConstructionContractor();
    await loadDesignUnit();
    await loadUtilities();
    await loadRegionLink();
}

async function loadProvince() {
    let provinceData = await apiHelper.getDataFromUrl(BASE_URL_API_PROVINCE + "/all");
    controlHelper.setDatasourceSelectElement("#customModal1 #province", provinceData.body, "id", "name", SELECTED_OBJECT != null ? SELECTED_OBJECT.province.id : null);
}

async function loadDistrict() {
    let districtData = await apiHelper.getDataFromUrl(BASE_URL_API_DISTRICT + "/byProvinceId/" + $("#customModal1 #province").val());
    controlHelper.setDatasourceSelectElement("#customModal1 #district", districtData.body, "id", "name", SELECTED_OBJECT != null ? SELECTED_OBJECT.district.id : null);
}

async function loadWard() {
    let wardData = await apiHelper.getDataFromUrl(BASE_URL_API_WARD + "/byDistrictId/" + $("#customModal1 #district").val());
    controlHelper.setDatasourceSelectElement("#customModal1 #ward", wardData.body, "id", "name", SELECTED_OBJECT != null ? SELECTED_OBJECT.ward.id : null);
}

async function loadStreet() {
    let streetData = await apiHelper.getDataFromUrl(BASE_URL_API_STREET + "/byProvinceIdAndDistrictId/" + $("#customModal1 #province").val() + "/" + $("#customModal1 #district").val());
    controlHelper.setDatasourceSelectElement("#customModal1 #street", streetData.body, "id", "name", SELECTED_OBJECT != null ? SELECTED_OBJECT.street.id : null);
}

async function loadInvestor() {
    let investorData = await apiHelper.getDataFromUrl(BASE_URL_API_INVESTOR + "/all");
    controlHelper.setDatasourceSelectElement("#customModal1 #investor", investorData.body, "id", "name", SELECTED_OBJECT != null ? SELECTED_OBJECT.investor.id : null);
}

async function loadConstructionContractor() {
    let constructionContractorData = await apiHelper.getDataFromUrl(BASE_URL_API_CONSTRUCTION_CONTRACTOR + "/all");
    controlHelper.setDatasourceSelectElement("#customModal1 #constructionContractor", constructionContractorData.body, "id", "name", SELECTED_OBJECT != null ? SELECTED_OBJECT.constructionContractor.id : null);
}

async function loadDesignUnit() {
    let designUnitData = await apiHelper.getDataFromUrl(BASE_URL_API_DESIGN_UNIT + "/all");
    controlHelper.setDatasourceSelectElement("#customModal1 #designUnit", designUnitData.body, "id", "name", SELECTED_OBJECT != null ? SELECTED_OBJECT.designUnit.id : null);
}

async function loadUtilities() {
    let utilitiesData = await apiHelper.getDataFromUrl(BASE_URL_API_UTILITIES + "/all");
    controlHelper.setDatasourceSelectElement("#customModal1 #utilities", utilitiesData.body, "id", "name", SELECTED_OBJECT != null ? SELECTED_OBJECT.utilities.id : null);
}

async function loadRegionLink(){
    let regionLinkData = await apiHelper.getDataFromUrl(BASE_URL_API_REGION_LINK + "/all");
    controlHelper.setDatasourceSelectElement("#customModal1 #regionLink", regionLinkData.body, "id", "name", SELECTED_OBJECT != null ? SELECTED_OBJECT.regionLink.id : null);
}

function getDataObjectFromModal() {
    let data = {
        name: $("#name").val(),
        province: { id: $("#customModal1 #province").val() },
        district: { id: $("#customModal1 #district").val() },
        ward: { id: $("#customModal1 #ward").val() },
        street: { id: $("#customModal1 #street").val() },
        address: $("#customModal1 #address").val(),
        slogan: $("#customModal1 #slogan").val(),
        description: $("#customModal1 #description").val(),
        acreage: $("#customModal1 #acreage").val(),
        constructArea: $("#customModal1 #constructArea").val(),
        numBlock: $("#customModal1 #numBlock").val(),
        numFloors: $("#customModal1 #numFloors").val(),
        numApartment: $("#customModal1 #numApartment").val(),
        apartmenttArea: $("#customModal1 #apartmenttArea").val(),
        investor: { id: $("#customModal1 #investor").val() },
        constructionContractor: { id: $("#customModal1 #constructionContractor").val() },
        designUnit: { id: $("#customModal1 #designUnit").val() },
        utilities: $("#customModal1 #utilities").val().toString(),
        regionLink: $("#customModal1 #regionLink").val().toString(),
        photo: $("#customModal1 #photo").val(),
        lat: $("#customModal1 #lat").val(),
        lng: $("#customModal1 #lng").val()
    }

    return data;
}

function getUtilitiesString(utilities) {
    let utilitiesString = "";
    for (let i = 0; i < utilities.length; i++) {
        utilitiesString += utilities[i].name + ",";
    }
    return utilitiesString;
}

async function createMethod() {
    let requestObject = getDataObjectFromModal();
    console.log(requestObject);
    //console.log requestObject as json string
    console.log(JSON.stringify(requestObject));

    console.log(BASE_URL_API + "/create");
    let response = await fetch(BASE_URL_API + "/create", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(requestObject),
    });

    let data = await response.json();
    if (response.ok) {
        console.log(data);
        alert(data.message);
        loadDataFromDatabase();
        CM.closeModal("#customModal1");
    }
    else {
        console.log(data);
        alert(data.message + "\n" + responseHandler.getErrorFromResponse(data));
    }
}

async function updateMethod(id) {
    let requestObject = getDataObjectFromModal();
    let projectId = $("#project-id").val();
    console.log(requestObject);

    let response = await fetch(BASE_URL_API + "/update/" + id, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(requestObject),
    });

    let data = await response.json();
    if (response.ok) {
        console.log(data);
        alert(data.message);
        loadDataFromDatabase();
        CM.closeModal("#customModal1");
    }
    else {
        console.log(data);
        alert(data.message + "\n" + responseHandler.getErrorFromResponse(data));
    }
}

function deleteMethod(id) {
    fetch(BASE_URL_API + "/delete/" + id, {
        method: "DELETE",
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            alert(data.message);
            loadDataFromDatabase();
            CM.closeModal("#customModal1");
        })
        .catch(error => {
            console.error(error);
        });
}

function deleteAllMethod() {
    fetch(BASE_URL_API + "/deleteall", {
        method: "DELETE",
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            alert(data.message);
            loadDataFromDatabase();
            CM.closeModal("#customModal1");
        })
        .catch(error => {
            console.error(error);
        });
}

import * as CM from "../CustomModal.js";
import * as adminLteHelper from "../adminlte-utils.js";
import * as responseHandler from "../responseHandler.js";
import { User } from "../user.js";


$(document).ready(function () {
    const ACTIVE_MENU_CONTENT = "Sign up";
    adminLteHelper.setActiveMenu(ACTIVE_MENU_CONTENT);
    adminLteHelper.setActiveBreadcrumbItemContent(ACTIVE_MENU_CONTENT);
    adminLteHelper.setContentHeader(ACTIVE_MENU_CONTENT);
    //adminLteHelper.loadContent("views/loginView.html");
    // add event listener for registerModal <a> tag

    addEventListener();

});

function addEventListener() {
    $("#signup").on("click", function () {
        signUp();
    });
}

function signUp() {
    var user = new User();
    user.name = $("#name").val();
    user.email = $("#email").val();
    user.password = $("#password").val();
    user.confirmPassword = $("#confirm-password").val();

    console.log(user);
    if (!user.isValidPasswordAndConfirmPassword()) {
        alert("Password and Confirm Password must be the same");
        return;
    }

    fetch("http://localhost:8080/api/auth/signup", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(user)
    })
        .then(response => response.json())
        .then(data => responseHandler.showResponseMessage(data));

}





// var modal1 = new CM.CustomModal("Login");
// modal1.show();
// modal1.addBodyRow("Username", CM.INPUTTYPE.INPUT, "username");
// modal1.addBodyRow("Password", CM.INPUTTYPE.PASSWORD, "password");
// modal1.addBodyRow("Email", CM.INPUTTYPE.SELECT, "select");
// modal1.addFooterButton("submit", "Login", "btn btn-primary");
// console.log("login.js");
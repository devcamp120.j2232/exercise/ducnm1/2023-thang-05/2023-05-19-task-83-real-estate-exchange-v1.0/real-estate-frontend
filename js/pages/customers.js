import * as adminLteHelper from '../adminlte-utils.js';
import * as dataTableHelper from '../datatable-utils.js';
import * as CM from "../CustomModal.js"
import * as responseHandler from "../responseHandler.js"

const ACTIVE_MENU_CONTENT = "Khách hàng";
const DOMAIN = "http://localhost:8080";
const BASE_URL_API = DOMAIN + "/customer";
var SELECTED_OBJECT = null;

$(document).ready(function () {
    setContent();
    addEventListener();
    initDataTable();
    loadDataFromDatabase();
});

function setContent() {
    adminLteHelper.setActiveMenu(ACTIVE_MENU_CONTENT);
    adminLteHelper.setActiveBreadcrumbItemContent(ACTIVE_MENU_CONTENT);
    adminLteHelper.setContentHeader(ACTIVE_MENU_CONTENT);
}

function addEventListener() {
    $("#btn-show-modal-addnew").on("click", function () { showModalAddNew(); });
    $("#table").on("click", ".action-edit", function () { showModalEdit(this); });
    $("#table").on("click", ".action-delete", function () { showModalDelete(this); });
    $("#btn-show-modal-delete-all").on("click", function () { showModalDeleteAll(); });
}

function initDataTable() {
    $("#table").DataTable({
        columns: [
            { data: "id", defaultContent: "" },
            { data: "contactName", defaultContent: "" },
            { data: "contactTitle", defaultContent: "" },
            { data: "address", defaultContent: "" },
            { data: "mobile", defaultContent: "" },
            { data: "email", defaultContent: "" },
            { data: "note", defaultContent: "" },
            { data: "createBy", defaultContent: "" },
            { data: "updateBy", defaultContent: "" },
            { data: "createDate", defaultContent: "" },
            { data: "updateDate", defaultContent: "" },
            {
                defaultContent:
                    `
                    <i class="fa fa-edit action-edit" style="cursor:pointer;color:blue;margin-right:10px"></i>
                    <i class="fa fa-trash action-delete" style="cursor:pointer;color:red"></i>
                    `
            }
        ]
    });
}

function loadDataFromDatabase() {
    fetch(BASE_URL_API + "/all")
        .then(response => response.json())
        .then(data => {
            console.log(data);
            dataTableHelper.setDatasource($("#table").DataTable(), data.body);
        })
        .catch(error => {
            console.error(error);
        });
}

function showModalAddNew() {
    var cm = new CM.CustomModal("Thêm mới");
    cm.addBodyRow("Tên chủ nhà", CM.INPUTTYPE.INPUT_TEXT, "contactName");
    cm.addBodyRow("Contact title", CM.INPUTTYPE.INPUT_TEXT, "contactTitle");
    cm.addBodyRow("Địa chỉ", CM.INPUTTYPE.INPUT_TEXT, "address");
    cm.addBodyRow("Điện thoại", CM.INPUTTYPE.INPUT_TEXT, "mobile");
    cm.addBodyRow("Email", CM.INPUTTYPE.INPUT_TEXT, "email");
    cm.addBodyRow("Note", CM.INPUTTYPE.INPUT_TEXT, "note");

    cm.addFooterButton("Thêm mới", "btn btn-success", function () { createMethod(); });

    cm.show();
}

async function showModalEdit(sender) {
    let selectedRow = $(sender).parents("tr");
    let selectedObject = $("#table").DataTable().row(selectedRow).data();
    SELECTED_OBJECT = selectedObject;
    console.log(selectedObject);

    var cm = new CM.CustomModal("Chi tiết");
    cm.addBodyRow("Tên chủ nhà", CM.INPUTTYPE.INPUT_TEXT, "contactName", selectedObject.contactName);
    cm.addBodyRow("Contact title", CM.INPUTTYPE.INPUT_TEXT, "contactTitle", selectedObject.contactTitle);
    cm.addBodyRow("Địa chỉ", CM.INPUTTYPE.INPUT_TEXT, "address", selectedObject.address);
    cm.addBodyRow("Điện thoại", CM.INPUTTYPE.INPUT_TEXT, "mobile", selectedObject.mobile);
    cm.addBodyRow("Email", CM.INPUTTYPE.INPUT_TEXT, "email", selectedObject.email);
    cm.addBodyRow("Note", CM.INPUTTYPE.INPUT_TEXT, "note", selectedObject.note);

    cm.addFooterButton("Lưu lại", "btn btn-primary", function () { updateMethod(selectedObject.id); });

    cm.show();
}

function showModalDelete(sender) {
    let selectedRow = $(sender).parents("tr");
    let selectedObject = $("#table").DataTable().row(selectedRow).data();

    var cm = new CM.CustomModal("Xóa");
    cm.addBodyText("Bạn có chắc chắn muốn xóa?");
    cm.addFooterButton("Xóa", "btn btn-danger", function () { deleteMethod(selectedObject.id); });
    cm.show();
}

function showModalDeleteAll() {
    var cm = new CM.CustomModal("Xóa tất cả");
    cm.addBodyText("Bạn có chắc chắn muốn xóa tất cả?");
    cm.addFooterButton("Xóa", "btn btn-danger", function () { deleteAllMethod(); });
    cm.show();
}

function addDatasourceAndSetValueSelectElement(elementSelector) {
    fetch(DOMAIN + "/addressmap/all")
        .then(response => response.json())
        .then(data => {
            console.log(data);
            let selectElement = $(elementSelector);
            data.forEach(element => {
                selectElement.append(`<option value="${element.id}">${element.address}</option>`);
            });
            try {
                selectElement.val(SELECTED_OBJECT.address.id);
            } catch (error) {
                //selectElement.val(0);
            }
        })
        .catch(error => {
            console.error(error);
        });
}

function getDataObjectFromModal() {
    let data = {
        contactName: $("#contactName").val(),
        contactTitle: $("#contactTitle").val(),
        address: $("#address").val(),
        mobile: $("#mobile").val(),
        email: $("#email").val(),
        note: $("#note").val(),
    }
    return data;
}

async function createMethod() {
    let requestObject = getDataObjectFromModal();
    let projectId = $("#project-id").val();
    console.log(requestObject);

    let response = await fetch(BASE_URL_API + "/create", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(requestObject),
    });

    let data = await response.json();
    if (response.ok) {
        console.log(data);
        alert(data.message);
        loadDataFromDatabase();
        CM.closeModal("#customModal1");
    }
    else {
        console.log(data);
        alert(data.message + "\n" + responseHandler.getErrorFromResponse(data));
    }
}

async function updateMethod(id) {
    let requestObject = getDataObjectFromModal();
    let projectId = $("#project-id").val();
    console.log(requestObject);

    let response = await fetch(BASE_URL_API + "/update/" + id, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(requestObject),
    });

    let data = await response.json();
    if (response.ok) {
        console.log(data);
        alert(data.message);
        loadDataFromDatabase();
        CM.closeModal("#customModal1");
    }
    else {
        console.log(data);
        alert(data.message + "\n" + responseHandler.getErrorFromResponse(data));
    }
}

function deleteMethod(id) {
    fetch(BASE_URL_API + "/delete/" + id, {
        method: "DELETE",
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            alert(data.message);
            loadDataFromDatabase();
            CM.closeModal("#customModal1");
        })
        .catch(error => {
            console.error(error);
        });
}

function deleteAllMethod() {
    fetch(BASE_URL_API + "/deleteall", {
        method: "DELETE",
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            alert(data.message);
            loadDataFromDatabase();
            CM.closeModal("#customModal1");
        })
        .catch(error => {
            console.error(error);
        });
}

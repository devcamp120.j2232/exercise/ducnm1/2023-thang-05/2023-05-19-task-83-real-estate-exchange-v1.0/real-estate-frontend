import * as adminLteHelper from '../adminlte-utils.js';
import * as dataTableHelper from '../datatable-utils.js';
import * as CM from "../CustomModal.js"
import * as responseHandler from "../responseHandler.js"

const ACTIVE_MENU_CONTENT = "Kết nối vùng";
const DOMAIN = "http://localhost:8080";
const BASE_URL_API = DOMAIN + "/regionlink";
var SELECTED_OBJECT = null;

$(document).ready(function () {
    setContent();
    addEventListener();
    initDataTable();
    loadDataFromDatabase();
});

function setContent() {
    adminLteHelper.setActiveMenu(ACTIVE_MENU_CONTENT);
    adminLteHelper.setActiveBreadcrumbItemContent(ACTIVE_MENU_CONTENT);
    adminLteHelper.setContentHeader(ACTIVE_MENU_CONTENT);
}

function addEventListener() {
    $("#btn-show-modal-addnew").on("click", function () { showModalAddNew(); });
    $("#table").on("click", ".action-edit", function () { showModalEdit(this); });
    $("#table").on("click", ".action-delete", function () { showModalDelete(this); });
    $("#btn-show-modal-delete-all").on("click", function () { showModalDeleteAll(); });
}

function initDataTable() {
    $("#table").DataTable({
        columns: [
            { data: "id", defaultContent: "" },
            { data: "name", defaultContent: "" },
            { data: "description", defaultContent: "" },
            { data: "photo", defaultContent: "" },
            { data: "address", defaultContent: "" },
            { data: "lat", defaultContent: "" },
            { data: "lng", defaultContent: "" },
            {
                defaultContent:
                    `
                    <i class="fa fa-edit action-edit" style="cursor:pointer;color:blue;margin-right:10px"></i>
                    <i class="fa fa-trash action-delete" style="cursor:pointer;color:red"></i>
                    `
            }
        ]
    });
}

function loadDataFromDatabase() {
    fetch(BASE_URL_API + "/all")
        .then(response => response.json())
        .then(data => {
            console.log(data);
            dataTableHelper.setDatasource($("#table").DataTable(), data.body);
        })
        .catch(error => {
            console.error(error);
        });
}

function showModalAddNew() {
    var cm = new CM.CustomModal("Thêm mới");
    cm.addBodyRow("Tên", CM.INPUTTYPE.INPUT_TEXT, "name");
    cm.addBodyRow("Mô tả", CM.INPUTTYPE.TEXTAREA, "description");
    cm.addBodyRow("Ảnh", CM.INPUTTYPE.INPUT_FILE, "photo");
    cm.addBodyRow("Địa chỉ", CM.INPUTTYPE.INPUT_TEXT, "address");
    cm.addBodyRow("Vĩ độ lat", CM.INPUTTYPE.INPUT_TEXT, "lat");
    cm.addBodyRow("Kinh độ lng", CM.INPUTTYPE.INPUT_TEXT, "lng");


    cm.addFooterButton("Thêm mới", "btn btn-success", function () { createMethod(); });

    // addDatasourceAndSetValueSelectElement("#address");
    cm.show();
}

async function showModalEdit(sender) {
    let selectedRow = $(sender).parents("tr");
    let selectedObject = $("#table").DataTable().row(selectedRow).data();
    SELECTED_OBJECT = selectedObject;

    var cm = new CM.CustomModal("Chi tiết");
    cm.addBodyRow("Tên", CM.INPUTTYPE.INPUT_TEXT, "name", selectedObject.name);
    cm.addBodyRow("Mô tả", CM.INPUTTYPE.TEXTAREA, "description", selectedObject.description);
    cm.addBodyRow("Ảnh", CM.INPUTTYPE.INPUT_FILE, "photo", selectedObject.photo);
    cm.addBodyRow("Địa chỉ", CM.INPUTTYPE.INPUT_TEXT, "address", selectedObject.address);
    cm.addBodyRow("Vĩ độ lat", CM.INPUTTYPE.INPUT_TEXT, "lat", selectedObject.lat);
    cm.addBodyRow("Kinh độ lng", CM.INPUTTYPE.INPUT_TEXT, "lng", selectedObject.lng);

    cm.addFooterButton("Lưu lại", "btn btn-primary", function () { updateMethod(selectedObject.id); });

    //addDatasourceAndSetValueSelectElement("#address");

    cm.show();
}

function showModalDelete(sender) {
    let selectedRow = $(sender).parents("tr");
    let selectedObject = $("#table").DataTable().row(selectedRow).data();

    var cm = new CM.CustomModal("Xóa");
    cm.addBodyText("Bạn có chắc chắn muốn xóa?");
    cm.addFooterButton("Xóa", "btn btn-danger", function () { deleteMethod(selectedObject.id); });
    cm.show();
}

function showModalDeleteAll() {
    var cm = new CM.CustomModal("Xóa tất cả");
    cm.addBodyText("Bạn có chắc chắn muốn xóa tất cả?");
    cm.addFooterButton("Xóa", "btn btn-danger", function () { deleteAllMethod(); });
    cm.show();
}

function addDatasourceAndSetValueSelectElement(elementSelector) {
    fetch(DOMAIN + "/addressmap/all")
        .then(response => response.json())
        .then(data => {
            console.log(data);
            let selectElement = $(elementSelector);
            data.forEach(element => {
                selectElement.append(`<option value="${element.id}">${element.address}</option>`);
            });
            try {
                selectElement.val(SELECTED_OBJECT.address.id);
            } catch (error) {
                //selectElement.val(0);
            }
        })
        .catch(error => {
            console.error(error);
        });
}

function getDataObjectFromModal() {
    let data = {
        name: $("#name").val(),
        description: $("#description").val(),
        photo: $("#photo").val(),
        address: $("#address").val(),
        lat: $("#lat").val(),
        lng: $("#lng").val(),
    }
    return data;
}

async function createMethod() {
    let requestObject = getDataObjectFromModal();
    let projectId = $("#project-id").val();
    console.log(requestObject);

    let response = await fetch(BASE_URL_API + "/create", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(requestObject),
    });

    let data = await response.json();
    if (response.ok) {
        console.log(data);
        alert(data.message);
        loadDataFromDatabase();
        CM.closeModal("#customModal1");
    }
    else {
        console.log(data);
        alert(data.message + "\n" + responseHandler.getErrorFromResponse(data));
    }
}

async function updateMethod(id) {
    let requestObject = getDataObjectFromModal();
    let projectId = $("#project-id").val();
    console.log(requestObject);

    let response = await fetch(BASE_URL_API + "/update/" + id, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(requestObject),
    });

    let data = await response.json();
    if (response.ok) {
        console.log(data);
        alert(data.message);
        loadDataFromDatabase();
        CM.closeModal("#customModal1");
    }
    else {
        console.log(data);
        alert(data.message + "\n" + responseHandler.getErrorFromResponse(data));
    }
}

function deleteMethod(id) {
    fetch(BASE_URL_API + "/delete/" + id, {
        method: "DELETE",
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            alert(data.message);
            loadDataFromDatabase();
            CM.closeModal("#customModal1");
        })
        .catch(error => {
            console.error(error);
        });
}

function deleteAllMethod() {
    fetch(BASE_URL_API + "/deleteall", {
        method: "DELETE",
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            alert(data.message);
            loadDataFromDatabase();
            CM.closeModal("#customModal1");
        })
        .catch(error => {
            console.error(error);
        });
}

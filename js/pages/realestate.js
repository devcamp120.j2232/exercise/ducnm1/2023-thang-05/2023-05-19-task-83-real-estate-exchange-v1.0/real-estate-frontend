import * as adminLteHelper from '../adminlte-utils.js';
import * as dataTableHelper from '../datatable-utils.js';
import * as CM from "../CustomModal.js"
import * as responseHandler from "../responseHandler.js"
import * as controlHelper from "../controlHelper.js"
import * as apiHelper from "../apiHelper.js"

const ACTIVE_MENU_CONTENT = "Thông tin BĐS";
const DOMAIN = "http://localhost:8080";
const BASE_URL_API = DOMAIN + "/realestate";
const BASE_URL_API_PROVINCE = DOMAIN + "/province";
const BASE_URL_API_DISTRICT = DOMAIN + "/district";
// const BASE_URL_API_WARD = DOMAIN + "/ward";
// const BASE_URL_API_STREET = DOMAIN + "/street";
// const BASE_URL_API_INVESTOR = DOMAIN + "/investor";
// const BASE_URL_API_CONSTRUCTION_CONTRACTOR = DOMAIN + "/constructioncontractor";
// const BASE_URL_API_DESIGN_UNIT = DOMAIN + "/designunit";
// const BASE_URL_API_UTILITIES = DOMAIN + "/utilities";
// const BASE_URL_API_REGION_LINK = DOMAIN + "/regionlink";


var SELECTED_OBJECT = null;

$(document).ready(function () {
    setContent();
    addEventListener();
    initDataTable();
    loadDataFromDatabase();
});

function setContent() {
    adminLteHelper.setActiveMenu(ACTIVE_MENU_CONTENT);
    adminLteHelper.setActiveBreadcrumbItemContent(ACTIVE_MENU_CONTENT);
    adminLteHelper.setContentHeader(ACTIVE_MENU_CONTENT);
}

function addEventListener() {
    $("#btn-show-modal-addnew").on("click", function () { showModalAddNew(); });
    $("#table").on("click", ".action-edit", function () { showModalEdit(this); });
    $("#table").on("click", ".action-delete", function () { showModalDelete(this); });
    $("#btn-show-modal-delete-all").on("click", function () { showModalDeleteAll(); });


}

function initDataTable() {
    $("#table").DataTable({
        columns: [
            /*
            *public class RealEstate {
                @Id
                @GeneratedValue(strategy = GenerationType.IDENTITY)
                private int id;
            
                @Column(nullable = true)
                private String title;
            
                @Column(nullable = true)
                private int type;
            
                @Column(nullable = true)
                private int request;
            
                @ManyToOne
                @JoinColumn(name = "province_id", nullable = true)
                private Province province;
            
                @ManyToOne
                @JoinColumn(name = "district_id", nullable = true)
                private District district;
            
                @ManyToOne
                @JoinColumn(name = "wards_id", nullable = true)
                private Ward ward;
            
                @ManyToOne
                @JoinColumn(name = "street_id", nullable = true)
                private Street street;
            
                @ManyToOne
                @JoinColumn(name = "project_id", nullable = true)
                private Project project;
            
                @Column(nullable = false)
                private String address;
            
                @ManyToOne
                @JoinColumn(name = "customer_id", nullable = true)
                private Customer customer;
            
                @Column(nullable = true)
                private long price;
            
                @Column(nullable = true)
                private long price_min;
            
                @Column(nullable = true)
                private int price_time;
            
                @Column(nullable = false)
                @JsonFormat(pattern = "yyyy-MM-dd")
                private Date date_create;
            
                @Column(nullable = true)
                private double acreage;
            
                @Column(nullable = true)
                private int direction;
            
                @Column(nullable = true)
                private int total_floors;
            
                @Column(nullable = true)
                private int number_floors;
            
                @Column(nullable = true)
                private int bath;
            
                @Column(nullable = true)
                private String apart_code;
            
                @Column(nullable = true)
                private double wall_area;
            
                @Column(nullable = true)
                private int bedroom;
            
                @Column(nullable = true)
                private int balcony;
            
                @Column(nullable = true)
                private String landscape_view;
            
                @Column(nullable = true)
                private int apart_loca;
            
                @Column(nullable = true)
                private int apart_type;
            
                @Column(nullable = true)
                private int furniture_type;
            
                @Column(nullable = true)
                private int price_rent;
            
                @Column(nullable = true)
                private double return_rate;
            
                @Column(nullable = true)
                private int legal_doc;
            
                @Column(nullable = true)
                private String description;
            }
            */

            { data: "id", defaultContent: "" },
            { data: "title", defaultContent: "" },
            { data: "type", defaultContent: "" },
            { data: "request", defaultContent: "" },
            { data: "province", defaultContent: "" },
            { data: "district", defaultContent: "" },
            { data: "wards", defaultContent: "" },
            { data: "street", defaultContent: "" },
            { data: "project", defaultContent: "" },
            { data: "address", defaultContent: "" },
            { data: "customer", defaultContent: "" },
            { data: "price", defaultContent: "" },
            { data: "price_min", defaultContent: "" },
            { data: "price_time", defaultContent: "" },
            { data: "date_create", defaultContent: "" },
            { data: "acreage", defaultContent: "" },
            { data: "direction", defaultContent: "" },
            { data: "total_floors", defaultContent: "" },
            { data: "number_floors", defaultContent: "" },
            { data: "bath", defaultContent: "" },
            { data: "apart_code", defaultContent: "" },
            { data: "wall_area", defaultContent: "" },
            { data: "bedroom", defaultContent: "" },
            { data: "balcony", defaultContent: "" },
            { data: "landscape_view", defaultContent: "" },
            { data: "apart_loca", defaultContent: "" },
            { data: "apart_type", defaultContent: "" },
            { data: "furniture_type", defaultContent: "" },
            { data: "price_rent", defaultContent: "" },
            { data: "return_rate", defaultContent: "" },
            { data: "legal_doc", defaultContent: "" },
            { data: "description", defaultContent: "" },


            {
                defaultContent:
                    `
                    <i class="fa fa-edit action-edit" style="cursor:pointer;color:blue;margin-right:10px"></i>
                    <i class="fa fa-trash action-delete" style="cursor:pointer;color:red"></i>
                    `
            }
        ]


    });
}

function loadDataFromDatabase() {
    fetch(BASE_URL_API + "/all")
        .then(response => response.json())
        .then(data => {
            console.log(data);
            dataTableHelper.setDatasource($("#table").DataTable(), data.body);
        })
        .catch(error => {
            console.error(error);
        });
}

async function showModalAddNew() {
    var cm = new CM.CustomModal("Thêm mới");
    cm.addBodyRow("Tiền tố", CM.INPUTTYPE.INPUT_TEXT, "prefix");
    cm.addBodyRow("Tên đường", CM.INPUTTYPE.INPUT_TEXT, "name");
    cm.addBodyRow("Tỉnh thành phố", CM.INPUTTYPE.SELECT, "province");
    cm.addBodyRow("Quận Huyện", CM.INPUTTYPE.SELECT, "district");
    cm.addFooterButton("Thêm mới", "btn btn-success", function () { createMethod(); });
    addDatasourceAndSetValueSelectElement();
    cm.show();
}

async function showModalEdit(sender) {
    let selectedRow = $(sender).parents("tr");
    let selectedObject = $("#table").DataTable().row(selectedRow).data();
    SELECTED_OBJECT = selectedObject;
    console.log(selectedObject);

    var cm = new CM.CustomModal("Chi tiết");
    cm.addBodyRow("Tiền tố", CM.INPUTTYPE.INPUT_TEXT, "prefix", selectedObject.prefix);
    cm.addBodyRow("Tên đường", CM.INPUTTYPE.INPUT_TEXT, "name", selectedObject.name);
    cm.addBodyRow("Tỉnh thành phố", CM.INPUTTYPE.SELECT, "province", selectedObject.province.id);
    cm.addBodyRow("Quận Huyện", CM.INPUTTYPE.SELECT, "district", selectedObject.district.id);

    cm.addFooterButton("Lưu lại", "btn btn-primary", function () { updateMethod(selectedObject.id); });
    addDatasourceAndSetValueSelectElement();
    cm.show();
}

function showModalDelete(sender) {
    let selectedRow = $(sender).parents("tr");
    let selectedObject = $("#table").DataTable().row(selectedRow).data();

    var cm = new CM.CustomModal("Xóa");
    cm.addBodyText("Bạn có chắc chắn muốn xóa?");
    cm.addFooterButton("Xóa", "btn btn-danger", function () { deleteMethod(selectedObject.id); });
    cm.show();
}

function showModalDeleteAll() {
    var cm = new CM.CustomModal("Xóa tất cả");
    cm.addBodyText("Bạn có chắc chắn muốn xóa tất cả?");
    cm.addFooterButton("Xóa", "btn btn-danger", function () { deleteAllMethod(); });
    cm.show();
}

async function addDatasourceAndSetValueSelectElement() {
    $("#customModal1 #province").on("change", async function () {
        await loadDistrict();
        await loadWard();
        await loadStreet();
    });
    $("#customModal1 #district").on("change", async function () {
        await loadWard();
        await loadStreet();
    });
    $("#customModal1 #ward").on("change", async function () {
        await loadStreet();
    });

    await loadProvince();
    await loadDistrict();
}

async function loadProvince() {
    let provinceData = await apiHelper.getDataFromUrl(BASE_URL_API_PROVINCE + "/all");
    controlHelper.setDatasourceSelectElement("#customModal1 #province", provinceData.body, "id", "name", SELECTED_OBJECT != null ? SELECTED_OBJECT.province.id : null);
}

async function loadDistrict() {
    let districtData = await apiHelper.getDataFromUrl(BASE_URL_API_DISTRICT + "/byProvinceId/" + $("#customModal1 #province").val());
    controlHelper.setDatasourceSelectElement("#customModal1 #district", districtData.body, "id", "name", SELECTED_OBJECT != null ? SELECTED_OBJECT.district.id : null);
}

async function loadWard() {
    let wardData = await apiHelper.getDataFromUrl(BASE_URL_API_WARD + "/byDistrictId/" + $("#customModal1 #district").val());
    controlHelper.setDatasourceSelectElement("#customModal1 #ward", wardData.body, "id", "name", SELECTED_OBJECT != null ? SELECTED_OBJECT.ward.id : null);
}

async function loadStreet() {
    let streetData = await apiHelper.getDataFromUrl(BASE_URL_API_STREET + "/byProvinceIdAndDistrictId/" + $("#customModal1 #province").val() + "/" + $("#customModal1 #district").val());
    controlHelper.setDatasourceSelectElement("#customModal1 #street", streetData.body, "id", "name", SELECTED_OBJECT != null ? SELECTED_OBJECT.street.id : null);
}

async function loadInvestor() {
    let investorData = await apiHelper.getDataFromUrl(BASE_URL_API_INVESTOR + "/all");
    controlHelper.setDatasourceSelectElement("#customModal1 #investor", investorData.body, "id", "name", SELECTED_OBJECT != null ? SELECTED_OBJECT.investor.id : null);
}

async function loadConstructionContractor() {
    let constructionContractorData = await apiHelper.getDataFromUrl(BASE_URL_API_CONSTRUCTION_CONTRACTOR + "/all");
    controlHelper.setDatasourceSelectElement("#customModal1 #constructionContractor", constructionContractorData.body, "id", "name", SELECTED_OBJECT != null ? SELECTED_OBJECT.constructionContractor.id : null);
}

async function loadDesignUnit() {
    let designUnitData = await apiHelper.getDataFromUrl(BASE_URL_API_DESIGN_UNIT + "/all");
    controlHelper.setDatasourceSelectElement("#customModal1 #designUnit", designUnitData.body, "id", "name", SELECTED_OBJECT != null ? SELECTED_OBJECT.designUnit.id : null);
}

async function loadUtilities() {
    let utilitiesData = await apiHelper.getDataFromUrl(BASE_URL_API_UTILITIES + "/all");
    controlHelper.setDatasourceSelectElement("#customModal1 #utilities", utilitiesData.body, "id", "name", SELECTED_OBJECT != null ? SELECTED_OBJECT.utilities.id : null);
}

async function loadRegionLink() {
    let regionLinkData = await apiHelper.getDataFromUrl(BASE_URL_API_REGION_LINK + "/all");
    controlHelper.setDatasourceSelectElement("#customModal1 #regionLink", regionLinkData.body, "id", "name", SELECTED_OBJECT != null ? SELECTED_OBJECT.regionLink.id : null);
}

function getDataObjectFromModal() {
    let data = {
        prefix: $("#customModal1 #prefix").val(),
        name: $("#customModal1 #name").val(),
        district: {
            id: $("#customModal1 #district").val()
        },
        province: {
            id: $("#customModal1 #province").val()
        }
    }

    return data;
}

function getUtilitiesString(utilities) {
    let utilitiesString = "";
    for (let i = 0; i < utilities.length; i++) {
        utilitiesString += utilities[i].name + ",";
    }
    return utilitiesString;
}

async function createMethod() {
    let requestObject = getDataObjectFromModal();
    console.log(requestObject);
    //console.log requestObject as json string
    console.log(JSON.stringify(requestObject));

    console.log(BASE_URL_API + "/create");
    let response = await fetch(BASE_URL_API + "/create", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(requestObject),
    });

    let data = await response.json();
    if (response.ok) {
        console.log(data);
        alert(data.message);
        loadDataFromDatabase();
        CM.closeModal("#customModal1");
    }
    else {
        console.log(data);
        alert(data.message + "\n" + responseHandler.getErrorFromResponse(data));
    }
}

async function updateMethod(id) {
    let requestObject = getDataObjectFromModal();
    let projectId = $("#project-id").val();
    console.log(requestObject);

    let response = await fetch(BASE_URL_API + "/update/" + id, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(requestObject),
    });

    let data = await response.json();
    if (response.ok) {
        console.log(data);
        alert(data.message);
        loadDataFromDatabase();
        CM.closeModal("#customModal1");
    }
    else {
        console.log(data);
        alert(data.message + "\n" + responseHandler.getErrorFromResponse(data));
    }
}

function deleteMethod(id) {
    fetch(BASE_URL_API + "/delete/" + id, {
        method: "DELETE",
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            alert(data.message);
            loadDataFromDatabase();
            CM.closeModal("#customModal1");
        })
        .catch(error => {
            console.error(error);
        });
}

function deleteAllMethod() {
    fetch(BASE_URL_API + "/deleteall", {
        method: "DELETE",
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            alert(data.message);
            loadDataFromDatabase();
            CM.closeModal("#customModal1");
        })
        .catch(error => {
            console.error(error);
        });
}

export async function getDataFromUrl(url) {
    let response = await fetch(url);
    let data = await response.json();
    return data;
}
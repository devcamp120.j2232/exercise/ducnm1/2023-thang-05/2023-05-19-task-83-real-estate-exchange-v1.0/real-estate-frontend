export function setActiveMenu(menuContent) {
    $('.nav-link').filter(function () {
        return $(this).find('p').text() === menuContent;
    }).addClass("active");
}

export function setActiveBreadcrumbItemContent(breadcrumbContent) {
    const activeBreadcrumbItem = document.querySelector("li.breadcrumb-item.active");
    if (activeBreadcrumbItem) {
        activeBreadcrumbItem.textContent = breadcrumbContent;
    } else {
        console.error("No active breadcrumb item found.");
    }
}

export function setContentHeader(contentHeader) {
    const contentHeaderElement = document.querySelector(".content-header h1");
    if (contentHeaderElement) {
        contentHeaderElement.textContent = contentHeader;
    } else {
        console.error("No content header element found.");
    }
}

export function loadContent(contentUrl) {
    const contentDiv = document.querySelector(".content");
    fetch(contentUrl)
        .then(response => response.text())
        .then(data => {
            //get content of body tag
            const regex = /<body[^>]*>((.|[\n\r])*)<\/body>/im;
            const result = regex.exec(data);
            //replace content of body tag with content of body tag of loginView.html 
            contentDiv.innerHTML = result[1];
        })
        .catch(error => {
            console.error(error);
        });
}


export class User {
    constructor(name, email, password, confirmPassword) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    isValidPasswordAndConfirmPassword() {
        return this.password == this.confirmPassword;
    }
}